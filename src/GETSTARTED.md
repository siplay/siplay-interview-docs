# Get Started

## Installation
Make sure you have a recent (>=6.4.0) version of [NodeJS](http://nodejs.org) installed.

```
node --version
```

Clone this repository.

```
git clone git@bitbucket.org:siplay/siplay-interview-angularjs.git
```

Install NPM dependencies.
```
npm install
```

## Development: Dev Server

Start the dev server.
```
npm start
```

Open your browser to http://localhost:3000.

The dev server will automatically recompile any source change and refresh the browser.

## Development: Tests

Run tests
```
npm test
```

Watch tests
```
npm run test:watch
```