# Overview



## The Stack

This exercise uses a number of common tools to improve the development experience.

* [SASS](http://sass-lang.com/documentation/file.SASS_REFERENCE.html) - CSS Extension/Preprocessor
* [Pug](https://pugjs.org/api/getting-started.html) - HTML Template Engine
* [Bootstrap 4](https://v4-alpha.getbootstrap.com/getting-started/introduction/) - Responsive UI Framework
* [Font Awesome](http://fontawesome.io/icons/) - Font-based CSS Icon Pack
* [Jasmine](https://jasmine.github.io/) - BDD-style Unit Testing Framework
* [Karma](https://karma-runner.github.io/1.0/index.html) - Test Runner
* [Webpack 2](https://webpack.js.org/configuration/) - bundling


