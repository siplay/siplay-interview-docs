# Coding Exercise: Task List

You will be implementing a simple task list app, with the following requirements:

* Write unit tests as appropriate.
* Display the list of tasks.
* Use the Dev REST API to persist and query task data (see Dev API documentation).
* Provide a field to add new tasks.
* Allow tasks to be toggled as "done" or "not done".
* "done" tasks must be styled with a ~~strike through~~.
* Allow the title of tasks to be edited.
* Allow tasks to be deleted from the list.
* Allow tasks to be reordered.