#SIPlay Interview Docs

This is a repository used to share common documentation for frontend developer candidate interview code exercises.

To publish an update, commit your changes, and then run `npm version VERSION_TYPE` where `VERSION_TYPE` is one of
`major`, `minor`, `patch`, `premajor`, `preminor`, `prepatch`, or `prerelease`.  